---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 5
title: Política
created: 1332707586
date: 2012-03-25
aliases:
- "/node/5/"
- "/page/5/"
---

## Administração Pública

* [Administração Pública]({{< ref "/politica/ap" >}})
* [Agenda Digital]({{< ref "/politica/agenda-digital" >}})
* [Concursos Públicos]({{< ref "/politica/concursos-publicos" >}})
* [Manifesto do Campo das Cebolas]({{< ref "/politica/campo-das-cebolas" >}})
* [Monitorização do RNID]({{< ref "/iniciativas/monitorizacao-rnid" >}})


## Direito de Autor

* [Direito de Autor]({{< ref "/politica/da" >}})
* [ACTA]({{< ref "/politica/acta" >}})
* [Cópia Privada]({{< ref "/politica/copia-privada" >}})
  * [Apresentação à CECC]({{< ref "/politica/copia-privada-prescecc20120208" >}})
  * [Consulta Pública Europeia de 2012]({{< ref "/politica/copia-privada-consulta-publica-2012" >}})
* [DRM]({{< ref "/iniciativas/diz-nao-ao-drm" >}})
* [EUCD]({{< ref "/politica/eucd" >}})


## Outros

* [Patentes]({{< ref "/politica/swpat" >}})
* [Legislação]({{< ref "/politica/legislacao" >}})
* [15 de Outubro]({{< ref "/politica/15O" >}})
* [Comércio Electrónico](https://github.com/marado/ecommerce-WTO/blob/master/ecommerce.md)
