---
categories:
- outros
metadata:
  servicos:
  - servicos_tid: 4
  site:
  - site_url: https://www.dognaedis.com/
    site_title: ''
    site_attributes: a:0:{}
  node_id: 194
layout: servicos
title: Dognædis
created: 1372270717
date: 2013-06-26
aliases:
- "/node/194/"
- "/servicos/194/"
---
<p>Empresa Portuguesa que trabalha na área da Segurança Informática.</p>
