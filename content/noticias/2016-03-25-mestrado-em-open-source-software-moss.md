---
categories:
- formação
metadata:
  servicos:
  - servicos_tid: 3
  site:
  - site_url: http://moss.dcti.iscte.pt/
    site_title: ''
    site_attributes: a:0:{}
  node_id: 407
layout: servicos
title: Mestrado em Open Source Software (MOSS)
created: 1458945673
date: 2016-03-25
aliases:
- "/node/407/"
- "/servicos/407/"
---
<p>O <strong>Mestrado em Software de Código Aberto</strong>, também designado <strong>Mestrado em Open Source Software (MOSS)</strong>, aborda temas específicos relativos ao desenvolvimento e exploração de tecnologias de informação no âmbito do software livre e de código aberto (FOSS – Free and Open Source Software) . Nesse contexto, são transmitidos conhecimentos quer das áreas de Informática (Sistemas Operativos, Programação, Redes ou Bases de Dados) quer de Economia e Gestão aplicados ao contexto do software livre e de código aberto. O mestrado é centrado num conjunto de tecnologias reconhecidas (e.g. GNU/Linux, Java, PHP ou GIT), sendo ainda uma oportunidade para discussão e apresentação de tecnologias emergentes (e.g. Nodejs, NoSQL, IoT).</p>
