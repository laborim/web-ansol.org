---
categories:
- newsletter
layout: article
title: Contributo da ANSOL sobre Direitos de Autor para o Parlamento
date: 2022-10-10
image:
  caption: |
    Palácio de São Bento,
    por [Manuel Menal](https://www.flickr.com/photos/mmenal/9307363528),
    sob a licença [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)
---

A ANSOL foi convidada a enviar um contributo sobre Direitos de Autor para a
Comissão de Cultura, Comunicação, Juventude e Desporto da Assembleia da
República, no âmbito da conferência a realizar no próximo dia 11 de outubro. A
[inscrição para o evento público][inscricao] e o [programa do evento][programa]
estão disponíveis online.

O tema é crucial, dado que Portugal precisa de transpor para a lei nacional a
nova Diretiva Europeia 2019/790, relativa aos direitos de autor e direitos
conexos no mercado único digital, que resultou de um extenso debate devido a
alguns artigos que geraram polémica, como o artigo 17º (ex-artigo 13º), entre
outros.

O contributo da ANSOL começa por sublinhar a importância de chamar para a mesa
do debate entidades que representem os vários setores da sociedade que passaram
a ser impactados pelas leis de direito de autor nas últimas décadas, como os
cidadãos, as bibliotecas e entidades da sociedade civil também representantes
do património cultural, assim como entidades que representem os setores da
educação e da investigação científica, que representem órgãos de comunicação
social independentes e jornalistas, entre outros.

O parecer segue com várias propostas para a transposição dos vários artigos da
diretiva. No caso do artigo 17º, sugere-se uma transposição baseada na
interpretação do Tribunal de Justiça da União Europeia, com uma forte atenção
às salvaguardas dos direitos dos cidadãos e ao cuidado e restrição do uso de
filtros de bloqueio automáticos.

No artigo 15º, sugere-se uma atenção redobrada para permitir a partilha e
circulação das notícias, e portanto da necessária informação, quer às condições
dos jornalistas, que vêem os seus direitos diminuídos com este artigo.

Chamamos ainda a atenção da importância do artigo 25º da diretiva, que permite
que Portugal possa alargar e melhorar as exceções que tem e que são o garante
dos direitos fundamentais em várias áreas, desde o ensino e investigação
científica, à liberdade de expressão e opinião.

Damos um particular destaque aos artigos da diretiva para defender e proteger o
Domínio Público, crucial para o desenvolvimento e partilha cultural.

Por fim, sublinhamos a importância de explicitar na lei que as exceções ao
direito de autor (utilizações que os cidadãos podem fazer) não possam ser
eliminadas por cláusulas contratuais ou tecnológicas (como o DRM, por exemplo)
e que é importante que a lei nacional de 2017, que passou a permitir a quebra
de DRM para fins legais não seja enfraquecida, como a proposta de transposição
da diretiva enviada pelo Governo ao Parlamento o ano passado, tentava fazer.

Disponibilizamos online o texto integral do [contributo da ANSOL][contributo]
com as medidas propostas em detalhe.

[programa]: https://app.parlamento.pt/comissoes/programaA4conferencia11out2022.pdf
[inscricao]: https://app.parlamento.pt/InscriptionForm/form/FormularioA.aspx?formid=dirauteradignew
[contributo]: contributo.pdf
