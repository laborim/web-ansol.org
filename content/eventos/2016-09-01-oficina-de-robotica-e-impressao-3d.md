---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 446
  event:
    location: Águeda
    site:
      title: ''
      url: https://docs.google.com/forms/d/e/1FAIpQLScVtqG7qPr8OfBigmLdiLpWjFZP_kInjMPoBAARG243kwN2rA/viewform
    date:
      start: 2016-09-21 14:30:00.000000000 +01:00
      finish: 2016-09-21 17:30:00.000000000 +01:00
    map: {}
layout: evento
title: Oficina de Robótica e Impressão 3D
created: 1472739978
date: 2016-09-01
aliases:
- "/evento/446/"
- "/node/446/"
---
<p>Destinatários: 6 aos 15 anos</p>
