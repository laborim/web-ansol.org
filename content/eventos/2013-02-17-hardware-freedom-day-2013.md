---
excerpt: "<div style=\"float:left; padding-right:10px;\">\r\n\t<img alt=\"HFD 2013\"
  src=\"http://altlab.org/wp-content/uploads/2013/03/hfd-A4.png\" width=\"200px\"
  /></div>\r\n<p>O dia da liberdade do hardware vai ser celebrado um pouco por todo
  o mundo. Em Portugal a celebra&ccedil;&atilde;o ser&aacute; no AltLab em Lisboa:
  um dia de portas abertas para todos os quiserem aparecer, no dia 20 de Abril (S&aacute;bado)
  das 9 da manh&atilde; &agrave; meia-noite.</p>\r\n<p>Entre m&aacute;quinas CNC,
  robots e outras maravilhas do hardware, o AltLab ir&aacute; mostrar alguns projectos
  desenvolvidos pelos seus membros, que ir&atilde;o estar dispon&iacute;veis para
  os explicar a responder a quest&otilde;es.</p>\r\n<p>Se levar o seu hardware para
  mostrar e hackar, tanto melhor!</p>\r\n<p>Aparece!</p>\r\n<p>(Entrada gratuita,
  mas a inscri&ccedil;&atilde;o &eacute; obrigat&oacute;ria)</p>\r\n"
categories: []
metadata:
  node_id: 131
  event:
    location: AltLab - Lisboa, Prac­eta Domin­gos Rodrigues nº2, Figo Maduro, 2685-327
      Prior Velho
    site:
      title: ''
      url: http://altlab.org/2013/03/31/hardware-freedom-day-at-altlab-april-20/
    date:
      start: 2013-04-20 09:00:00.000000000 +01:00
      finish: 2013-04-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Hardware Freedom Day 2013
created: 1361066814
date: 2013-02-17
aliases:
- "/evento/131/"
- "/hfd2013/"
- "/node/131/"
---
<div style="float: right; padding-left: 10px;"><img src="http://altlab.org/wp-content/uploads/2013/03/hfd-A4.png" alt="HFD 2013" width="200px"></div><p>O dia da liberdade do hardware vai ser celebrado um pouco por todo o mundo. Em Portugal a celebração será no AltLab em Lisboa: um dia de portas abertas para todos os quiserem aparecer, no dia 20 de Abril (Sábado) das 9 da manhã à meia-noite.</p><p>Entre máquinas CNC, robots e outras maravilhas do hardware, o AltLab irá mostrar alguns projectos desenvolvidos pelos seus membros, que irão estar disponíveis para os explicar a responder a questões.</p><p>Se levar o seu hardware para mostrar e hackar, tanto melhor!</p><p>Aparece!</p><p>(Entrada gratuita, mas a inscrição é obrigatória)</p>
