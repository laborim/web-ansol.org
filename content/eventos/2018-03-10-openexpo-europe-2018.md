---
categories:
- open source
- free software
- empresas
- digitalização
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 122
  - tags_tid: 220
  - tags_tid: 221
  node_id: 553
  event:
    location: La Nave, Madrid, Espanha
    site:
      title: OpenExpo 2018
      url: https://openexpoeurope.com/
    date:
      start: 2018-06-06 13:45:00.000000000 +01:00
      finish: 2018-06-07 13:45:00.000000000 +01:00
    map: {}
layout: evento
title: OpenExpo Europe 2018
created: 1520690117
date: 2018-03-10
aliases:
- "/evento/553/"
- "/node/553/"
---
<div class="six columns"><div class="gdlr-item gdlr-content-item" style="margin-bottom: 40px;"><h4 style="text-align: justify;"><strong>OpenExpo Europe</strong> is the main professional Fair and Congress about Open Source, <strong>Free Software and Open World Economy (Open Data and Open Innovation)</strong> in Europe.</h4><h4 style="text-align: justify;">OpenExpo is the <strong>essential event for the technology and business leaders</strong> looking to develop, invest and implement <strong>Open Source technologies</strong>. OpenExpo is the<strong> meeting point of the sector</strong> for companies and entrepreneurs, engineers, designers, analysts, digital managers, developers and business managers among others, where companies <strong>expand their network of contacts, generate business and promote their services</strong>. The main suppliers of the sector will show their technological solutions for all types of companies. A unique showcase with the vision of experts and key market players. The aim of OpenExpo Europe is to <strong>share, present, discover and evaluate the Open Source solutions</strong> and the trends within the industry. Check out what’s new within the open technology world. Don’t miss the chance to discover the last trends to promote your business.</h4></div></div>
