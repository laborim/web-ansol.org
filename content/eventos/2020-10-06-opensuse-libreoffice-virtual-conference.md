---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 755
  event:
    location: 
    site:
      title: ''
      url: https://oslo.gonogo.live/accounts/login/?next=/
    date:
      start: 2020-10-13 00:00:00.000000000 +01:00
      finish: 2020-10-17 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: openSUSE + LibreOffice Virtual Conference
created: 1602002465
date: 2020-10-06
aliases:
- "/evento/755/"
- "/node/755/"
---

