---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 187
  event:
    location: Universidade Portucalense, Porto
    site:
      title: ''
      url: http://capsi2003.upt.pt/
    date:
      start: 2003-09-17 00:00:00.000000000 +01:00
      finish: 2003-09-17 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 4ª Conferência da Associação Portuguesa de Sistemas de Informação
created: 1371948968
date: 2013-06-23
aliases:
- "/evento/187/"
- "/node/187/"
---
<p>Jaime Villate (ANSOL) foi convidado a intervir numa sessão plenária sobre software livre, onde estarão presentes representates do INA e da Microsoft.</p>
