---
excerpt: "<p><img src=\"http://wiki.ansol.org/ConferenciaBlender?action=AttachFile&amp;do=get&amp;target=blenderconf.png\"
  /></p>\r\n<p>O Blender &eacute; um software livre de produ&ccedil;&atilde;o 3D dispon&iacute;vel
  em Windows, Macintosh, GNU/Linux e FreeBSD, e a sua aplica&ccedil;&atilde;o estende-se
  a v&aacute;rias &aacute;reas das quais se destacam os filmes, videojogos, publicidade
  e visualiza&ccedil;&atilde;o.</p>\r\n<p>Esta confer&ecirc;ncia pretende promover
  o Blender em Portugal como um valor acrescentado &agrave;s ferramentas propriet&aacute;rias.</p>\r\n<p>Este
  evento tem o apoio da&nbsp;<strong>ANSOL</strong>.</p>\r\n"
categories: []
metadata:
  node_id: 98
  event:
    location: Escola das Artes da Universidade Católica do Porto
    site:
      title: http://www.problender.pt/conf2013
      url: http://www.problender.pt/conf2013
    date:
      start: 2013-04-06 00:00:00.000000000 +01:00
      finish: 2013-04-07 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Conferência Blender PT 2013
created: 1351781543
date: 2012-11-01
aliases:
- "/evento/98/"
- "/node/98/"
---
<div style="float:right;">
	<img src="http://wiki.ansol.org/ConferenciaBlender?action=AttachFile&amp;do=get&amp;target=blenderconf.png" /></div>
<p>O Blender &eacute; um software livre de produ&ccedil;&atilde;o 3D dispon&iacute;vel em Windows, Macintosh, GNU/Linux e FreeBSD, e a sua aplica&ccedil;&atilde;o estende-se a v&aacute;rias &aacute;reas das quais se destacam os filmes, videojogos, publicidade e visualiza&ccedil;&atilde;o.</p>
<p>Esta confer&ecirc;ncia pretende promover o Blender em Portugal como um valor acrescentado &agrave;s ferramentas propriet&aacute;rias.</p>
<p>Investigadores, empres&aacute;rios e freelancers ir&atilde;o aprofundar o debate acerca do real impacto do Blender a n&iacute;vel acad&eacute;mico e&nbsp; profissional. A organiza&ccedil;&atilde;o est&aacute; a cargo da <a href="http://problender.pt/">PROBlender - Associa&ccedil;&atilde;o&nbsp; Portuguesa de Blender</a>, em parceria com a Escola das Artes e o Centro de&nbsp; Investiga&ccedil;&atilde;o em Ci&ecirc;ncia e Tecnologia das Artes (CITAR) da Universidade&nbsp; Cat&oacute;lica do Porto.</p>
<p>A PROBlender &eacute; constitu&iacute;da por membros do f&oacute;rum Blender PT, a comunidade para utilizadores de l&iacute;ngua portuguesa do Blender.</p>
<p>Este evento tem o apoio da <strong>ANSOL</strong>.</p>
