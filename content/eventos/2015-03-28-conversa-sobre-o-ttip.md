---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 301
  event:
    location: Barreiro
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/conversa-dia-31-de-marco-barreiro/
    date:
      start: 2015-03-31 21:00:00.000000000 +01:00
      finish: 2015-03-31 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: Conversa sobre o TTIP
created: 1427560935
date: 2015-03-28
aliases:
- "/evento/301/"
- "/node/301/"
---

