---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAD8+08iwPBEBU+HYENA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38754129293034e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9156219363213e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9156219363213e1
    mapa_top: !ruby/object:BigDecimal 27:0.38754129293034e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9156219363213e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38754129293034e2
    mapa_geohash: eyckrwuxht7czzpc
  slide:
  - slide_value: 0
  node_id: 630
  event:
    location: 
    site:
      title: ''
      url: http://dglab.gov.pt/evento-dglab-utilizacao-de-software-aberto-nas-organizacoes/
    date:
      start: 2018-11-15 00:00:00.000000000 +00:00
      finish: 2018-11-15 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Utilização de software aberto nas organizações
created: 1540551615
date: 2018-10-26
aliases:
- "/evento/630/"
- "/node/630/"
---
<p>A Direção Geral do Livro, dos Arquivos e das Bibliotecas (DGLAB) vai organizar nas instalações do Arquivo Nacional / Torre do Tombo, no próximo dia 15 de novembro, uma sessão dedicada à utilização de&nbsp;<em>software</em>&nbsp;Aberto (<em>open source</em>) pelas organizações.</p><p>Com este evento a DGLAB pretende:</p><ul><li>Explorar diversas experiência adquiridas em projetos de transferência para tecnologia de&nbsp;<em>software </em>Aberto;</li><li>Caraterizar, de forma alargada, vantagens e obstáculos passíveis de ser identificados em processos de transferência para tecnologias&nbsp;<em>software&nbsp;</em>Aberto;</li><li>Salientar a relevância da utilização de&nbsp;<em>software </em>Aberto em contextos culturais nomeadamente na preservação de informação digital;</li><li>Enquadrar os conceitos de Software Aberto, Interoperabilidade, Normas Abertas e Independência de Plataformas Tecnológicas, assim como promover o debate em torno destas temáticas.</li></ul>
