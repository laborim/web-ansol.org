---
categories:
- java career workshop
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 205
  node_id: 543
  event:
    location: ISCTE-IUL - Edifício II
    site:
      title: The Java Developer Career Workshop
      url: https://www.meetup.com/pt-jug/events/247782011/
    date:
      start: 2018-02-19 19:00:00.000000000 +00:00
      finish: 2018-02-19 21:00:00.000000000 +00:00
    map: {}
layout: evento
title: The Java Developer Career Workshop
created: 1518735327
date: 2018-02-15
aliases:
- "/evento/543/"
- "/node/543/"
---
<p>Hey PT.JUGgers! We are back again, and this time with the great Bruno Souza (aka Javaman), who will be doing a mini JUG tour in Europe.<br><br>Abstract: The Java Developer Career Workshop: work on the best projects and with the best teams!<br><br>Being a "rockstar" or a "celebrity" developer is not about being popular or giving presentations at big events. What sets those developers apart is the work they do on innovative projects and initiatives. By working with other great developers, they create a positive feedback loop that lifts their career into the stratosphere. Which comes first? Do great developers work on great projects, or are they made great by the projects they work on? On this interactive Java Developer Career Workshop you will improve the traits and skills that set "rockstar" developers apart. Together we will create the #1 thing that will start you into this positive feedback loop. You will understand the proven science of being an expert and how to apply to software development. And you will know exactly what you need to do to work on great projects (and why that does not means changing jobs!). Let's also discuss what is going on in the Java world, and what are new technologies trends and opportunities.<br><br>We are in the best career in history. Don't set yourself to be a mediocre developer: work on the best projects and with the best teams and become the developer you deserve to be!<br><br>Bio: Bruno Souza<br><br>Since 1995, Bruno (<a href="https://twitter.com/brjavaman" target="__blank" title="https://twitter.com/brjavaman" class="link">https://twitter.com/brjavaman</a>) helps Java developers improve their careers and work on cool projects with great people! Java Evangelist at ToolsCloud and Leader of SouJava, the Brazilian Java Users Society, Bruno discuss Java and the Software Developer Career in his <a href="https://code4.life" target="__blank" title="https://code4.life" class="link">https://code4.life</a> project.</p>
