---
categories:
- cópia privada
- debate
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 11
  - tags_tid: 48
  node_id: 230
  event:
    location: Lisboa, Streaming
    site:
      title: ''
      url: http://www.esquerda.net/artigo/bloco-promove-debate-sobre-copia-privada/34228
    date:
      start: 2014-09-28 21:00:00.000000000 +01:00
      finish: 2014-09-28 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Debate: “Cópia ou Partilha: Quem paga o quê?”'
created: 1411597817
date: 2014-09-24
aliases:
- "/evento/230/"
- "/node/230/"
---
<div class="field field-name-field-deck">Pedro Wallenstein, presidente da GDA - Direitos dos Artistas, e Paula Simões, da Associação Ensino Livre, trocam argumentos e esclarecem questões sobre a matéria em debate. “Cópia ou Partilha: Quem paga o quê?” será transmitido em direto pelo <a href="http://esquerda.net">http://esquerda.net</a> no próximo domingo, pelas 21h. Poderá enviar as suas questões para <a href="mailto:debate@esquerda.net">debate@esquerda.net</a>.</div>
