---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 809
  event:
    location: Online
    site:
      title: ''
      url: https://ec.europa.eu/eusurvey/runner/OSORwebinar5August2021
    date:
      start: 2021-08-05 00:00:00.000000000 +01:00
      finish: 2021-08-05 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Building cross-administration collaboration on OSS projects
created: 1626614177
date: 2021-07-18
aliases:
- "/evento/809/"
- "/node/809/"
---

