---
categories: []
metadata:
  node_id: 113
  event:
    location: Por todo o mundo
    site:
      title: ''
      url: http://www.publicdomainday.org/
    date:
      start: 2013-01-01 00:00:00.000000000 +00:00
      finish: 2013-01-01 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Dia do Domínio Público 2013
created: 1355264913
date: 2012-12-11
aliases:
- "/evento/113/"
- "/node/113/"
---
<p>A cada ano que passa, mais obras chegam finalmente ao Dom&iacute;nio P&uacute;blico. Para celebrar o dia em que o P&uacute;blico fica a ganhar, come&ccedil;ou-se a celebrar, de forma n&atilde;o organizada, o Dia do Dom&iacute;nio P&uacute;blico em 1 de Janeiro, um pouco por todo o mundo. Saiba mais em http://www.publicdomainday.org/</p>
