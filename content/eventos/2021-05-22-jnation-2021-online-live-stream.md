---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 802
  event:
    location: Online
    site:
      title: ''
      url: https://2021.jnation.pt/
    date:
      start: 2021-06-16 00:00:00.000000000 +01:00
      finish: 2021-06-16 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: JNation 2021 Online Live Stream
created: 1621718863
date: 2021-05-22
aliases:
- "/evento/802/"
- "/node/802/"
---
<p>JNation's Conference 4th edition is ONLINE and FREE to attend on the 16th of June.</p>
