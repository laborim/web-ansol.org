---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 316
  event:
    location: Porto
    site:
      title: 
      url: 
    date:
      start: 2015-05-14 18:00:00.000000000 +01:00
      finish: 2015-05-14 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Conferência: Alterações recentes (e vindouras?) ao Código do Direito de Autor'
created: 1429828673
date: 2015-04-23
aliases:
- "/evento/316/"
- "/node/316/"
---
<p>Nuno Sousa e Silva (FDUCP Porto) e Tito Rendas (FDUCP Lisboa) farão uma análise crítica ao recente pacote legislativo e discutirão possíveis alterações futuras na área do Direito de Autor. Em cima da mesa estarão, entre outros, os seguintes temas:</p><p>&nbsp;</p><p>- Cópia privada;</p><p>- Gestão colectiva;</p><p>- Obras órfãs;</p><p>- Tutela dos direitos de autor na Internet;</p><p>- Agregação de notícias.</p><p>&nbsp;</p><p>O evento terá lugar no Porto, no dia 14 de Maio, às 18:00h, no <em>campus</em> da Foz&nbsp;da Universidade Católica Portuguesa.</p>
