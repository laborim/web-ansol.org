---
categories:
- redes sociais
- voluntariado
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 225
  - tags_tid: 226
  node_id: 555
  event:
    location: Évora, Portugal
    site:
      title: Universidade de Évora
      url: http://www.cires.uevora.pt/
    date:
      start: 2018-06-07 14:00:00.000000000 +01:00
      finish: 2018-06-08 14:00:00.000000000 +01:00
    map: {}
layout: evento
title: 2º CIReS - Congresso Internacional de Redes Sociais
created: 1520690761
date: 2018-03-10
aliases:
- "/evento/555/"
- "/node/555/"
---
<div class="corpo"><p>O 2º Congresso Internacional de Redes Sociais, sob o tema “Redes Sociais: perspetivas e desafios emergentes nas sociedades contemporâneas” é um fórum internacional que congrega especialistas, investigadores e trabalhadores sociais que desenvolvem a sua atividade na área das redes sociais.</p><p>&nbsp;</p><p>Está aberta, <strong>até ao dia 10 de março</strong> a chamada para comunicações nas seguintes áreas temáticas.</p><p>&nbsp;</p><p>I. Saúde, bem-estar e estilos de vida<br> II. Educação, trabalho e cidadania<br> III. Redes de intervenção comunitária<br> IV. Redes sociais locais/municipais<br> V. Redes e destinos turísticos<br> VI. Mídias sociais<br> VII. Teorias, métodos e técnicas<br> VIII. Movimentos sociais e práticas culturais</p><p>&nbsp;</p><p>As normas para autores estão disponíveis em: <a href="http://www.cires.uevora.pt/registo-e-submissao/submissao/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=pt-PT&amp;q=http://www.cires.uevora.pt/registo-e-submissao/submissao/&amp;source=gmail&amp;ust=1518817317022000&amp;usg=AFQjCNGtrYEG58AP9XtXvf_EWfhexP-hOQ">http://www.cires.uevora.pt/registo-e-submissao/submissao/</a> .</p><p>&nbsp;</p><p>As <strong>propostas de comunicação devem ser submetidas no ficheiro disponível no site do 2ºCIReS</strong> e serão sujeitas a avaliação independente por dois especialistas, sob condições de duplo anonimato.</p><p>&nbsp;</p><p>Todos <strong>os trabalhos aceites serão publicados em livro de atas</strong>.</p><p>Mais esclarecimentos: <a href="mailto:cires@uevora.pt" target="_blank">cires@uevora.pt</a></p></div><div class="organizacao"><strong>Organização:</strong> <span>CICS.NOVA - Pólo da Universidade de Évora | Departamento de Sociologia da ECS/UÉ</span></div><div class="intervalo_datas"><span>De 07.06.2018 a 08.06.2018</span></div><div class="local"><span>Auditório do Colégio do Espírito Santo</span></div><div class="pagina_propria"><strong>Página:</strong> <a href="http://www.cires.uevora.pt/" target="_blank"> <span>http://www.cires.uevora.pt/</span> </a></div>
