---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 327
  event:
    location: Lisboa
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/debate-18-de-junho-lisboa/
    date:
      start: 2015-06-18 19:30:00.000000000 +01:00
      finish: 2015-06-18 19:30:00.000000000 +01:00
    map: {}
layout: evento
title: 'Debate: Tratado Transatlântico'
created: 1433090613
date: 2015-05-31
aliases:
- "/evento/327/"
- "/node/327/"
---

