---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 422
  event:
    location: 
    site:
      title: ''
      url: http://datewithdata.pt/
    date:
      start: 2016-05-14 00:00:00.000000000 +01:00
      finish: 2016-05-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Date With Data #8'
created: 1463154274
date: 2016-05-13
aliases:
- "/evento/422/"
- "/node/422/"
---
<p>Um ano depois do <a href="http://twitter.com/puredenoticias">Puré de Notícias</a>, voltamos a dedicar a nossa atenção ao texto. Os debates do Parlamento, um dataset que recolhemos e compilámos (usado no Demo.cratica), constituem uma base riquíssima para explorações semânticas da língua portuguesa. A curiosidade científica e precisa não é a nossa maior preocupação: gostamos de experiências e projetos alternativos como o <a href="https://openparliament.ca/labs/haiku/">gerador de haikus</a> a partir dos debates do parlamento canadiano.</p><p>A transcrição que começámos do guia de conversação português/inglês English as She is Spoke, vem também encaixar nesta linha de experimentação textual que temos trilhado e que vamos continuar a explorar.</p><p>O <a href="http://dre.tretas.org">Diário da República</a> é outra fonte enorme de texto burocrático que queremos tomar como plasticina para moldar em formatos curiosos, usando ferramentas e recursos como NLP, <em>machine learning</em>, correntes de Markov e outras maquinações. Se estes termos são alienígenas para ti, porque não aparecer para ficares a saber os usos criativos e interessantes que se podem dar a corpos de texto de todo o género?</p>
