---
categories: []
metadata:
  event_location:
  - event_location_value: Assembleia da República
  event_site:
  - event_site_url: http://app.parlamento.pt/webutils/docs/doc.pdf?Path=6148523063446f764c324679626d56304c334e706447567a4c31684a5355786c5a793944543030764d554e425130524d52793942636e463161585a765132397461584e7a5957387654334a6b5a57357a4947526c4946527959574a68624768764c304e425130524d52313878587a49344e4335775a47593d&Fich=CACDLG_1_284.pdf&Inline=true
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-01-21 10:00:00.000000000 +00:00
    event_start_value2: 2015-01-21 12:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 266
layout: evento
title: Apreciação e votação do relatório final da petição sobre Cópia Privada
created: 1421417674
date: 2015-01-16
---
<p>Constante da agenda da 1ª Comissão:</p><p>Apreciação e votação de relatórios finais de petições, designadamente: <br>Petição n.º 427/XII/4.ª - "Impedir a aprovação da Proposta de Lei n.º 246/XII - cópia privada"</p>
