---
categories:
- trends
- conference
- porto
metadata:
  event_location:
  - event_location_value: Alfândega do Porto, Porto
  event_site:
  - event_site_url: https://portotechhub.com/
    event_site_title: Porto TechHub 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-17 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-17 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 311
  - tags_tid: 290
  - tags_tid: 142
  node_id: 620
layout: evento
title: Porto TechHub 2018
created: 1525986359
date: 2018-05-10
---
Technological evolution and its transformations are undoubtedly changing the professional landscape. Understanding the challenges and projecting an image of the impact in future jobs are the objectives of the 4th edition of the Porto Tech Hub conference, which this year focuses on the theme “Future Jobs and Technologies”. The conference will also undergo structural changes. With two distinct stages, one dedicated to trending themes and the other focused mainly on the technical spectrum, the conference theme will be passionately debated by well-known pioneers on different technological fields.
Join us in shaping the future!
