---
categories:
- cópia privada
- debate
metadata:
  event_location:
  - event_location_value: Lisboa, Streaming
  event_site:
  - event_site_url: http://www.esquerda.net/artigo/bloco-promove-debate-sobre-copia-privada/34228
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-09-28 20:00:00.000000000 +01:00
    event_start_value2: 2014-09-28 20:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 11
  - tags_tid: 48
  node_id: 230
layout: evento
title: 'Debate: “Cópia ou Partilha: Quem paga o quê?”'
created: 1411597817
date: 2014-09-24
---
<div class="field field-name-field-deck">Pedro Wallenstein, presidente da GDA - Direitos dos Artistas, e Paula Simões, da Associação Ensino Livre, trocam argumentos e esclarecem questões sobre a matéria em debate. “Cópia ou Partilha: Quem paga o quê?” será transmitido em direto pelo <a href="http://esquerda.net">http://esquerda.net</a> no próximo domingo, pelas 21h. Poderá enviar as suas questões para <a href="mailto:debate@esquerda.net">debate@esquerda.net</a>.</div>
