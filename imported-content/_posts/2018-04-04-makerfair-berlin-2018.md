---
categories:
- maker
- open source
- free software
- hardware
- meeting
- fair
- feira
metadata:
  event_location:
  - event_location_value: FEZ-Berlin, Berlin, Alemanha
  event_site:
  - event_site_url: https://en.maker-faire.de/berlin/
    event_site_title: MakerFair Berlin
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-25 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-26 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 138
  - tags_tid: 127
  - tags_tid: 122
  - tags_tid: 136
  - tags_tid: 129
  - tags_tid: 267
  - tags_tid: 268
  node_id: 582
layout: evento
title: MakerFair Berlin 2018
created: 1522863343
date: 2018-04-04
---
<h3>A festival of invention, creativity and resourcefulness.</h3><p><big>Coming to&nbsp;Berlin for the fourth time in 2018! See you again on May 26 and 27, 2018 at our new venue: FEZ-Berlin!</big></p>
