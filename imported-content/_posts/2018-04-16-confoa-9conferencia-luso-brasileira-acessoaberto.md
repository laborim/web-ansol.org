---
categories:
- acesso aberto
- fct
- conferência
metadata:
  event_location:
  - event_location_value: ISCTE-IUL, Lisboa, Portugal
  event_site:
  - event_site_url: http://confoa.rcaap.pt/2018
    event_site_title: ConfOA 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-10-01 23:00:00.000000000 +01:00
    event_start_value2: 2018-10-03 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 299
  - tags_tid: 301
  - tags_tid: 293
  node_id: 611
layout: evento
title: ConfOA - 9ªConferência Luso-Brasileira AcessoAberto
created: 1523883005
date: 2018-04-16
---
<p>Dando continuidade ao êxito das edições anteriores e mantendo a parceria desde 2010,&nbsp;o evento é organizado&nbsp;pelos Serviços de Documentação da Universidade do Minho (<a href="http://www.sdum.uminho.pt/" target="_blank" rel="noopener">SDUM</a>), pela Fundação para a Ciência e a Tecnologia (<a href="http://www.fct.pt/" target="_blank" rel="noopener">FCT</a>), pelo Instituto Brasileiro de Informação em Ciência e Tecnologia (<a href="http://www.ibict.br/" target="_blank" rel="noopener">IBICT</a>) e pela Instituição que acolhe o evento que, de 2 a 4 de outubro de 2018, será o <a href="https://www.iscte-iul.pt/">ISCTE-IUL</a>, <a href="https://www.visitlisboa.com/pt-pt">Lisboa</a>.</p><p>A ConfOA, com realização alternada entre Portugal e Brasil, tem como objetivo reunir as comunidades portuguesa e brasileira, que desenvolvem atividades de investigação, desenvolvimento, gestão de serviços e definição de políticas relacionadas com o Acesso Aberto ao conhecimento e com a Ciência Aberta, com o propósito de promover a partilha, discussão e divulgação de conhecimentos, práticas e investigação sobres estas temáticas, em todas as suas dimensões e perspetivas.</p>
