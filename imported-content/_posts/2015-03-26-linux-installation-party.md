---
categories:
- lip
- evento
- linux installation party
metadata:
  event_location:
  - event_location_value: Departamento de Ciência dos Computadores da Faculdade de
      Ciências da Universidade do Porto
  event_site:
  - event_site_url: http://lip.alunos.dcc.fc.up.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-04-10 10:30:00.000000000 +01:00
    event_start_value2: 2015-04-10 17:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 91
  - tags_tid: 84
  - tags_tid: 92
  node_id: 298
layout: evento
title: Linux Installation Party
created: 1427412196
date: 2015-03-26
---
<p>Uma LIP é um evento para a divulgação do sistema operativo GNU/linux, um local para explorar, aprender e instalar Linux, sempre com um espírito de convívio e diversão.</p><p>Esta é organizada por alunos do Departamento de Ciência de Computadores da Faculdade de Ciências da Universidade do Porto para toda a comunidade da FCUP, com o imprescindível apoio do LabCC.</p><p>Este ano, a LIP será dia 10 de Abril, das 11h30 às 18h00 e será realizada no Departamento de Ciência dos Computadores da Faculdade de Ciências da Universidade do Porto</p>
