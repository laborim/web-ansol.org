---
categories:
- consultoria
- formação
- suporte
metadata:
  email:
  - email_email: info@eurotux.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 3
  - servicos_tid: 2
  site:
  - site_url: http://eurotux.com/
    site_title: http://eurotux.com/
    site_attributes: a:0:{}
  node_id: 42
layout: servicos
title: Eurotux - Informática, SA
created: 1334499855
date: 2012-04-15
---
<h2>
	Consultoria</h2>
<p>A Eurotux S.A. presta servi&ccedil;os de consultoria no planeamento, instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o de infra-estruturas de servi&ccedil;os de dados de pequena e larga escala.</p>
<p>A interven&ccedil;&atilde;o da Eurotux compreende o planeamento estrutural e de capacidade dos sistemas, o apoio &agrave; aquisi&ccedil;&atilde;o e instala&ccedil;&atilde;o de equipamento e o acompanhamento da evolu&ccedil;&atilde;o e manuten&ccedil;&atilde;o dos servi&ccedil;os.</p>
<p>Para o acesso e posicionamento exterior, s&atilde;o prestados servi&ccedil;os de planeamento estrat&eacute;gico e aconselhamento tecnol&oacute;gico com vista &agrave; explora&ccedil;&atilde;o das vantagens competitivas de uma presen&ccedil;a forte na Internet.</p>
<p>Ao n&iacute;vel local e intra-organizacional, s&atilde;o exploradas a versatilidade e seguran&ccedil;a das redes privadas virtuais e esta&ccedil;&otilde;es de trabalho de re-instala&ccedil;&atilde;o autom&aacute;tica. Na interface com o exterior, s&atilde;o promovidas solu&ccedil;&otilde;es modulares, seguras e economicamente competitivas baseadas em aplica&ccedil;&otilde;es l&iacute;deres no mercado.</p>
<p>Uma das principais voca&ccedil;&otilde;es da Eurotux &eacute; assegurar a seguran&ccedil;a de sistemas e redes inform&aacute;ticas contra ac&ccedil;&otilde;es il&iacute;citas de terceiros. Os ataques contra sistemas tem uma tend&ecirc;ncia constante a crescer e podem causar preju&iacute;zos muito avultados.</p>
<p>Na &oacute;ptica da infra-estrutura a Eurotux fornece solu&ccedil;&otilde;es &agrave; medida que englobam o planeamento da topologia das redes, a implanta&ccedil;&atilde;o de firewalls e a cria&ccedil;&atilde;o de circuitos seguros de comunica&ccedil;&atilde;o, como VPNs.</p>
<p>N&atilde;o menos importante do que a infra-estrutura s&atilde;o os servi&ccedil;os que permitem garantir a seguran&ccedil;a de forma continuada e reagir a situa&ccedil;&otilde;es que a possam p&ocirc;r em causa. Nestes salientam-se o servi&ccedil;o de actualiza&ccedil;&atilde;o permanente de vers&otilde;es de software, auditorias de seguran&ccedil;a em redes j&aacute; existentes, investiga&ccedil;&atilde;o de incidentes e os servi&ccedil;os de monitoriza&ccedil;&atilde;o permanente de actividade suspeita (managed security monitoring).</p>
<h2>
	Suporte</h2>
<p>A Eurotux S.A. proporciona aos seus clientes acompanhamento em todas as fases do projecto e disponibiliza servi&ccedil;os ininterruptos de manuten&ccedil;&atilde;o de operacionalidade ap&oacute;s a entrada do sistema em produ&ccedil;&atilde;o. Os servi&ccedil;os de manuten&ccedil;&atilde;o de operacionalidade compreendem a monitoriza&ccedil;&atilde;o global do sistema, interven&ccedil;&otilde;es preventivas de actualiza&ccedil;&atilde;o e correc&ccedil;&atilde;o, e a resposta a situa&ccedil;&otilde;es de conting&ecirc;ncia (com possibilidade de suporte 24x7 e contrata&ccedil;&atilde;o de SLAs). Complementarmente, &eacute; disponibilizado suporte &agrave; configura&ccedil;&atilde;o e administra&ccedil;&atilde;o de v&aacute;rias aplica&ccedil;&otilde;es: Apache (servidor web), PostgreSQL e MySQL (bases de dados), Samba, NFS e FTP (partilha e armazenamento de ficheiros), Qmail e Insight Server(messaging).</p>
<h2>
	Forma&ccedil;&atilde;o</h2>
<p>De forma a suportar a procura de conhecimentos t&eacute;cnicos sobre tecnologias Linux e Open Source, a Eurotux S.A. est&aacute; preparada para oferecer ac&ccedil;&otilde;es de forma&ccedil;&atilde;o e treino nas suas &aacute;reas de interven&ccedil;&atilde;o. No nosso portf&oacute;lio, destacam-se ac&ccedil;&otilde;es de forma&ccedil;&atilde;o nas seguintes &aacute;reas:</p>
<ul>
	<li>
		Linux B&aacute;sico: Instala&ccedil;&atilde;o, configura&ccedil;&atilde;o e administra&ccedil;&atilde;o de servidores Linux</li>
	<li>
		Linux InCorp: Instala&ccedil;&atilde;o, configura&ccedil;&atilde;o e administra&ccedil;&atilde;o de servi&ccedil;os intranet em Linux (DHCP, NAT, Proxy, SAMBA, NFS, messaging)</li>
	<li>
		Linux OutCorp: Instala&ccedil;&atilde;o, configura&ccedil;&atilde;o e administra&ccedil;&atilde;o de servi&ccedil;os Internet Linux (DNS, WWW/Apache, messaging, FTP, RSYNC).</li>
	<li>
		CVS: Controlo de revis&otilde;es de software com CVS.</li>
</ul>
<p>Ciente da import&acirc;ncia da comunidade de utilizadores Linux, a Eurotux est&aacute; atenta &agrave; &eacute;tica desse movimento, pelo que procurar&aacute; agir de forma a beneficiar toda a comunidade Linux. Al&eacute;m da comercializa&ccedil;&atilde;o de produtos Linux e solu&ccedil;&otilde;es baseadas em Linux, a empresa levar&aacute; a cabo actividades de divulga&ccedil;&atilde;o, incluindo cursos, demonstra&ccedil;&otilde;es e confer&ecirc;ncias. Sempre que poss&iacute;vel, o desenvolvimento de software pela Eurotux S.A. reger-se-&aacute; pela filosofia Open Source.</p>
