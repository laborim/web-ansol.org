---
categories: []
metadata:
  event_location:
  - event_location_value: Universidade Portucalense, Porto
  event_site:
  - event_site_url: http://www.bad.pt/noticia/2015/07/30/jornada-sobre-os-sistemas-de-informacao-em-open-source-em-setembro-no-porto/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-09-10 23:00:00.000000000 +01:00
    event_start_value2: 2015-09-10 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 340
layout: evento
title: 'Sistemas de informação em Open Source: partilha de experiências'
created: 1438354507
date: 2015-07-31
---

