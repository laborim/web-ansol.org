---
categories: []
metadata:
  event_site:
  - event_site_url: http://www.hfday.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-01-17 00:00:00.000000000 +00:00
    event_start_value2: 2015-01-17 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 249
layout: evento
title: Hardware Freedom Day
created: 1416578525
date: 2014-11-21
---
<p><img src="http://www.hfday.org/countdown/banner1-UTC-1-en.png" height="154" width="154"></p><p>A ANSOL está a estudar qual a melhor forma de marcar esta data. Tens uma ideia? Queres ajudar? Contacta-nos!</p>
