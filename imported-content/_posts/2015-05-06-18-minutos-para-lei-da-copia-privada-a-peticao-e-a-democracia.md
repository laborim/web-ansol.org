---
categories:
- cópia privada
- imprensa
metadata:
  tags:
  - tags_tid: 11
  - tags_tid: 19
  node_id: 317
layout: article
title: 18 minutos para Lei da Cópia Privada – a Petição e a Democracia
created: 1430952836
date: 2015-05-06
---
<p>O grupo de cidadãos que representou os mais de 8000 subscritores da petição “Impedir a aprovação da proposta de lei n° 246/XII, da Cópia Privada” [1] junto do Parlamento [2], vem manifestar publicamente o seu repúdio em relação ao inaceitável desrespeito pela democracia demonstrado neste processo pelos partidos da maioria. Foi agendada a discussão da supracitada petição para o mesmo dia da reapreciação do projeto de Lei, tendo sido destinado a ambas as atividades um tempo total de 18 minutos, 3 por grupo parlamentar. A discussão, se é que a tal chegará a curta conversa que a agenda prevê, terá lugar no dia 8 de Maio, durante a sessão plenária que se inicia às 10h.<br> <br> Por outras palavras, os partidos da maioria preparam-se para forçar a aprovação da Lei, remetida de volta ao Parlamento após justificado veto presidencial, fazendo da discussão da petição um mero expediente administrativo.<br> <br> A Lei vetada pelo Sr. Presidente da República aparece como resultado da negligência em cadeia de diversas instituições envolvidas: Conselho de Ministros, Comissão de Assuntos Constitucionais Direitos Liberdades e Garantias (CACDLG), Grupos Parlamentares e Presidência da Assembleia de República. Mas uma reapreciação forçada pós-veto presidencial será com grande probabilidade ação direta do líder do Governo.<br> <br> Num momento em que se discute a harmonização de políticas digitais a nível europeu, o Parlamento prepara-se para aprovar uma lei duvidosa nos objetivos, arbitrária nos pressupostos, obsoleta no contexto e danosa nos resultados [3]. Os portugueses ficarão mais longe de uma economia competitiva ao terem de suportar este novo imposto que tornará os dispositivos digitais muito mais caros em Portugal.<br> <br> &nbsp;Assim, repetimos a questão apresentada por várias associações nos últimos dias: a quem serve esta Lei? E porque razão se sente a maioria PSD / CDS-PP, contra o bom senso,a razão e um veto presidencial, na obrigação de aprová-la?<br> <br> [1] – <a href="http://www.peticaopublica.com/pview.aspx?pi=impedir-pl246" target="_blank">http://www.peticaopublica.com/pview.aspx?p</a><a href="http://www.peticaopublica.com/pview.aspx?pi=impedir-pl246" target="_blank">i=impedir-pl246</a><br> <br> [2] – <a href="https://www.youtube.com/watch?v=UN3hT2bIUOs" target="_blank">https://www.youtube.com/watch?v=UN3hT2bI</a><a href="https://www.youtube.com/watch?v=UN3hT2bIUOs" target="_blank">UOs</a><br> <br> [3] – <a href="http://jonasnuts.com/5-perguntas-dos-peticionarios-0-507278" target="_blank">http://jonasnuts.com/5-perguntas-dos-pet</a><a href="http://jonasnuts.com/5-perguntas-dos-peticionarios-0-507278" target="_blank">icionarios-0-507278</a></p>
