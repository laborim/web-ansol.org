---
categories: []
metadata:
  event_location:
  - event_location_value: Bairro Alto
  event_site:
  - event_site_url: http://www.openstreetmap.org/?mlat=38.71055&mlon=-9.142200&zoom=18&layers=M
    event_site_title: Ver no OpenStreetMap
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-04-28 20:30:00.000000000 +01:00
    event_start_value2: 2012-04-28 20:30:00.000000000 +01:00
  node_id: 58
layout: evento
title: Convívio com a FSF Europe
created: 1334873435
date: 2012-04-19
---
<p>Na sequ&ecirc;ncia da sua vinda a Lisboa para realiza&ccedil;&atilde;o da sua Assembleia Geral de 2012, vai ocorrer uma festa conv&iacute;vio da FSF Europe com ativistas de Software Livre.</p>
<p>O conv&iacute;vio vai decorrer depois do jantar no pr&oacute;ximo S&aacute;bado dia 28 de Abril.</p>
<p>Entre as <strong>21:30 e as 22:00</strong> estaremos ao p&eacute; da sa&iacute;da do Metro de Lisboa, <strong>Esta&ccedil;&atilde;o Baixa Chiado</strong>, <strong>sa&iacute;da do Largo do Chiado</strong>, provavelmente numa das esplanadas por tr&aacute;s da sa&iacute;da.</p>
<p>Depois seguiremos para a Galeria Z&eacute; dos Bois, onde em simult&acirc;neo ocorrer&aacute; um n&atilde;o relacionado <a href="http://www.zedosbois.org/events/matana-roberts/">concerto de Matana Roberts, &agrave;s 23h</a> (pagamento extra de 8&euro; no local para os interessados).</p>
<p>Se j&aacute; l&aacute; n&atilde;o estivermos, podes telefonar para: <strong>91 511 8425</strong> ou 93 32 55 619</p>
<p><img alt="m2.png" src="/sites/ansol.org/files/pictures/m2.png" style="width:732px;height:534px;" /></p>
