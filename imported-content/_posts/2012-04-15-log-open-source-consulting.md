---
categories:
- consultoria
metadata:
  email:
  - email_email: sales@log.pt
  servicos:
  - servicos_tid: 7
  site:
  - site_url: http://www.log.pt/
    site_title: http://www.log.pt/
    site_attributes: a:0:{}
  node_id: 53
layout: servicos
title: log - open source consulting
created: 1334501830
date: 2012-04-15
---
<p>Orientada para a consultoria em tecnologias Open Source, a log fornece um conjunto de estrat&eacute;gias, servi&ccedil;os e solu&ccedil;&otilde;es que cobrem a an&aacute;lise, concep&ccedil;&atilde;o, desenvolvimento, integra&ccedil;&atilde;o e implementa&ccedil;&atilde;o de solu&ccedil;&otilde;es tecnol&oacute;gicas em quatro &aacute;reas de neg&oacute;cio:</p>
<ul>
	<li>
		Desenvolvimento Aplicacional;</li>
	<li>
		Migra&ccedil;&atilde;o de Sistemas;</li>
	<li>
		Integra&ccedil;&atilde;o e Implementa&ccedil;&atilde;o;</li>
	<li>
		Infra-Estrutura.</li>
</ul>
