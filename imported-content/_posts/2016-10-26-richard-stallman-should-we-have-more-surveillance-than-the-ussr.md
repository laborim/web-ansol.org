---
categories:
- richard stallman
- stallman
- vigilância
metadata:
  event_location:
  - event_location_value: Auditório JJ Laginha (Edifício I), ISCTE, Lisboa
  event_site:
  - event_site_url: https://my.fsf.org/civicrm/profile/create?gid=50&reset=1
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-11-10 13:30:00.000000000 +00:00
    event_start_value2: 2016-11-10 15:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 185
  - tags_tid: 186
  - tags_tid: 187
  node_id: 471
layout: evento
title: 'Richard Stallman: Should We Have More Surveillance Than the USSR?'
created: 1477512098
date: 2016-10-26
---
<p>Richard Stallman, criador do movimento do Software Livre, está de volta a Portugal, e irá falar-nos sobre vigilância:</p><p><img src="https://ansol.org/sites/ansol.org/files/RMS2016.jpg" alt="poster of the event" title="poster of the event" style="display: block; margin-left: auto; margin-right: auto;" height="960" width="602"></p><p><strong>Should We Have More Surveillance Than the USSR?</strong></p><p>Digital technology has enabled governments to impose surveillance that Stalin could only dream of, making it next to impossible to talk with a reporter undetected. This puts democracy in danger. Stallman will present the absolute limit on general surveillance in a democracy, and suggest ways to design systems not to collect dossiers on all citizens.</p><hr><p><a href="http://iscte-iul.pt/quem_somos/localizacao.aspx">Localização: ISCTE</a></p><p>Presente no local do evento, irá estar uma banca com vários <a href="https://shop.fsf.org/">items da FSF</a>.</p>
