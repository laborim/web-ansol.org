---
excerpt: '<p style="margin: 0px 0px 0.75em; line-height: 1.5em;">A "<strong>ANSOL
  - Associação Nacional para o Software Livre</strong>" é uma associação portuguesa
  sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação
  e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas,
  culturais, técnicas e científicas.</p><p style="margin: 0px 0px 0.75em; line-height:
  1.5em;"><a href="http://ansol.org/filosofia/softwarelivre.pt.html" target="_self"
  title="O que é o Software Livre?" style="border-bottom: 1px solid #cccccc; color:
  #c00000; background-color: transparent; text-decoration: none;">O que é o Software
  Livre ?</a></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;"><a href="http://ansol.org/inscricao"
  target="_self" title="Inscrição na ANSOL" style="border-bottom: 1px solid #cccccc;
  color: #c00000; background-color: transparent; text-decoration: none;">Como tornar-me
  sócio ?</a></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;"><a href="http://ansol.org/docs/recursos_para_projectos_software_livre"
  target="_self" title="Recursos Software Livre" style="border-bottom: 1px solid #cccccc;
  color: #c00000; background-color: transparent; text-decoration: none;">Recursos
  disponíveis para projectos/eventos de Software Livre</a></p><p style="margin: 0px
  0px 0.75em; line-height: 1.5em;">Contacto:<span class="Apple-converted-space">&nbsp;</span><span
  class="link-mailto" style="padding: 1px 0px 1px 16px; background-color: transparent;
  background-position: 0px 1px;"><a href="mailto:contacto@ansol.org" style="border-bottom:
  1px solid #cccccc; color: #c00000; background-color: transparent; text-decoration:
  none;">contacto@ansol.org</a></span></p>'
categories: []
metadata:
  slide:
  - slide_value: 1
  node_id: 1
layout: page
title: Bem-vindo à ANSOL
created: 1332695512
date: 2012-03-25
---
<p style="margin: 0px 0px 0.75em; line-height: 1.5em;">A "<strong>ANSOL - Associação Nacional para o Software Livre</strong>" é uma associação portuguesa sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas, culturais, técnicas e científicas.</p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;"><a href="http://ansol.org/filosofia/softwarelivre.pt.html" target="_self" title="O que é o Software Livre?" style="border-bottom: 1px solid #cccccc; color: #c00000; background-color: transparent; text-decoration: none;">O que é o Software Livre ?</a></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;"><a href="http://ansol.org/inscricao" title="Inscrição na ANSOL" style="border-bottom: 1px solid #cccccc; color: #c00000; background-color: transparent; text-decoration: none;">Como tornar-me sócio ?</a></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;"><a href="http://ansol.org/docs/recursos_para_projectos_software_livre" target="_self" title="Recursos Software Livre" style="border-bottom: 1px solid #cccccc; color: #c00000; background-color: transparent; text-decoration: none;">Recursos disponíveis para projectos/eventos de Software Livre</a></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;">Contacto:<span class="Apple-converted-space">&nbsp;</span><span class="link-mailto" style="padding: 1px 0px 1px 16px; background-color: transparent; background-position: 0px 1px;"><a href="mailto:contacto@ansol.org" style="border-bottom: 1px solid #cccccc; color: #c00000; background-color: transparent; text-decoration: none;">contacto@ansol.org</a></span></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;">&nbsp;<span class="link-mailto" style="padding: 1px 0px 1px 16px; background-color: transparent; background-position: 0px 1px;"><img src="https://ansol.org/sites/ansol.org/files/a5frenteWeb.png" alt="Inscreva-se!" style="vertical-align: middle;" height="800"></span></p><p style="margin: 0px 0px 0.75em; line-height: 1.5em;"><span class="link-mailto" style="padding: 1px 0px 1px 16px; background-color: transparent; background-position: 0px 1px;">A ANSOL é associada da <a href="http://fsfe.org/">FSFE</a>.</span></p>
