---
categories:
- linux
- linz
metadata:
  event_location:
  - event_location_value: Wissensturm & Hackerspace /dev/lol, Linz, Austria
  event_site:
  - event_site_url: https://www.linuxwochen-linz.at/2018/home/
    event_site_title: Linuxwochen Linz 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-06-22 23:00:00.000000000 +01:00
    event_start_value2: 2018-06-23 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 274
  node_id: 587
layout: evento
title: Linuxwochen Linz 2018
created: 1522874377
date: 2018-04-04
---
<p><strong>Achtung Datumsänderung: Leider mussten wir die Veranstaltung vom Mai auf das o.g. Datum verschieben.</strong></p><p>Auch 2018 laden die Linuxwochen Linz wieder alle Technikinteressierte und Freunde der Open Source/Open Hardware/Open Data Welt ein am Samstag den 23. und Sonntag den 24. Juni dabei zu sein.</p><p>Ein Teil der Vorträge werden in Form eines Barcamp organisiert. Teilnahme am Barcamp ist bis unmittelbar auf der Veranstaltung möglich.</p><p>Konferenzsprache: Deutsch (Englische Vorträge möglich)<br> Ort: Linz (Wissensturm &amp; Hackerspace /dev/lol)<br> <strong>Eintritt frei!</strong><br> <strong>Keine Anmeldung erforderlich.</strong></p>
