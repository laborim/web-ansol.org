---
categories:
- hardware
- open cv
- raspberry pi
- workshop
metadata:
  event_location:
  - event_location_value: Águeda, Portugal
  event_site:
  - event_site_url: http://us7.campaign-archive2.com/?u=2e765ab173a4f6c47a88ff607&id=a9b735efda&e=1f6e401a32
    event_site_title: Workshop Visão Artificial ALL Águeda
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-21 14:30:00.000000000 +00:00
    event_start_value2: 2015-11-21 14:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 136
  - tags_tid: 147
  - tags_tid: 148
  - tags_tid: 149
  node_id: 376
layout: evento
title: Workshop de Visão Artificial com Utilização de Raspberry Pi - Agueda Living
  Lab
created: 1445886749
date: 2015-10-26
---
<p>No próximo dia 21 de novembro, o ALL em parceria com a VisionMaker irá promover um Workshop de Visão Artificial, com utilização de Raspberry Pi, que irá decorrer na Incubadora de Empresas de Águeda (Rua Luís de Camões) a partir das 14h30.<br>A visão artificial carateriza-se essencialmente pela utilização de imagens digitais para reconhecimento de padrões, sendo cada vez mais utilizada em diversas áreas, nomeadamente, na Medicina.<br>Neste workshop será possível ter uma breve introdução ao conceito, analisando os algoritmos utilizados, softwares existentes e as aplicações da visão artificial, bem como fazer exercícios práticos com utilização de Raspberry Pi.</p><p>Nota: Os participantes devem trazer computador.</p>
