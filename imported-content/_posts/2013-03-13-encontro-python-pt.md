---
categories: []
metadata:
  event_location:
  - event_location_value: Multiversus, Rua do Bonjardim 618 Loja C, Porto
  event_site:
  - event_site_url: https://plus.google.com/events/cps7aef27ikh9m6j97vc9cvob68
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-04-05 23:00:00.000000000 +01:00
    event_start_value2: 2013-04-05 23:00:00.000000000 +01:00
  node_id: 134
layout: evento
title: Encontro Python.pt
created: 1363217293
date: 2013-03-13
---
<p>Programa:</p>
<ul>
	<li>
		11:00 - Talk: Usar Sentry (http://getsentry.com) para gest&atilde;o de exce&ccedil;&otilde;es em projetos (Jos&eacute; Moreira /@zemanel) </li>
	<li>
		11:30 - Talk: Web2py (Francisco Costa /@franciscosta) </li>
	<li>
		12:00 - Talk: a definir </li>
	<li>
		12:30 - Talk: a definir </li>
	<li>
		13:00 - almo&ccedil;o</li>
</ul>
